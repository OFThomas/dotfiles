######################################################################
# @author      : oli (oli@s3rv3r)
# @file        : ubuntu_server_install
# @created     : Saturday Nov 12, 2022 22:38:37 GMT
#
# @description : 
######################################################################
sudo apt-get update
sudo apt-get dist-upgrade -y

# git bash setup
sudo apt-get install -y git vim-gtk3 exuberant-ctags

# vimrc/bashrc and vim plugin setup 
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ln -sf ~/dotfiles/bashrc ~/.bashrc
ln -sf ~/dotfiles/vimrc ~/.vimrc
ln -sf ~/dotfiles/colors/ ~/.vim/colors

#then you can open vim and do 
#   :PluginInstall 

sudo apt-get install -y lolcat screenfetch

sudo apt-get install -y cmake g++ clang 

sudo apt-get install -y lm-sensors

sudo ubuntu-drivers autoinstall 
