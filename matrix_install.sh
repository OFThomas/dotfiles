######################################################################
# @author      : oli (oli@oli-desktop)
# @file        : matrix_install
# @created     : Sunday Oct 02, 2022 22:07:14 BST
#
# @description : 
######################################################################


# repo at https://github.com/joschuck/matrix-webcam
pip install matrix-webcam
sudo apt install -y v4l2loopback-dkms v4l2loopback-utils wmctrl
sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
