# Glab

It's pretty neat :).
See
`https://glab.readthedocs.io/en/latest/index.html`

## General usage 

`glab label ls`
`glab label create -n test`

`glab issue list`
`glab issue view -c #`
`glab issue note #`
`glab issue board view #`
`glab issue create`

`glab pipeline ci view`

`glab mr view`
`glab mr approve`
`glab mr merge`
`glab mr list`

`glab mr create`
`glab mr for #`

## To install
Install from snap
`sudo snap install --edge glab`
`sudo snap connect glab:ssh-keys`

To set up permissions, add a personal access token on gitlab 
`https://gitlab.com/-/profile/personal_access_tokens`

Then run 
`glab auth login`

Lastly 
`glab config set --global editor vim`

# Using Git properly 
`git shortlog`

The differences between two branches
`git shortlog main..branch`

Files which have changed between branches
`git diff --name-status main`


# SpaceVim config
<!-- cp init.toml ~/.SpaceVim.d/init.toml -->
ln -sf ~/dotfiles/init.toml ~/.SpaceVim.d/init.toml
ln -sf ~/dofiles/myspacevim.vim ~/.SpaceVim.d/autoload/myspacevim.vim

# To install neovim using the appimage and spaceVim
1. Remove old configs 
```
rm -rf ~/.vim* ~/.config/*vim* ~/.config/*Vim* ~/.SpaceVim ~/.cache/*vim* ~/.cache/*Vim* ~/.cache/neosnippet ~/.local/share/*vim*
```

2. Install neovim appimage
```
wget https://github.com/neovim/neovim/releases/download/v0.8.3/nvim.appimage
```
3. Make it executable
```
chmod u+x nvim.appimage
```
4. Move it to a folder in your path
```
sudo ln -s /path/to/nvim.appimage /usr/bin/nvim
```
5. Reinstall SpaceVim
```
curl -sLf https://spacevim.org/install.sh | bash
```
# To install ltex-ls
Get the release tarball from the website
extract it with tar xvzf ltex.gz
Then copy the folder to your path somewhere, add the ltex/bin directory to your
path
i.e.
fish_add_path ~/.local/bin/ltex/bin
