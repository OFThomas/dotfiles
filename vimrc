set nocompatible              " be iMproved, required
filetype off                  " required

filetype plugin on
syntax on

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" Declare the list of plugins.
" "latex
Plug 'lervag/vimtex'
Plug 'tpope/vim-sensible'
" file dir in vim
Plug 'preservim/nerdtree' "{ 'on': 'NERDTreeToggle'}
" Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" tabs
Plug 'godlygeek/tabular'
" colour!
Plug 'flazz/vim-colorschemes'
Plug 'rfunix/vim-greenisgood'
" org mode
Plug 'jceb/vim-orgmode'
" speeddating...
Plug 'tpope/vim-speeddating'
Plug 'xolox/vim-easytags'

Plug 'xolox/vim-misc'

Plug 'tibabit/vim-templates'

Plug 'tpope/vim-fugitive'
" Plug 'airblade/vim-gitgutter'
Plug 'mhinz/vim-signify'

" Plug 'dense-analysis/ale'
" Plug 'dense-analysis/ale', { 'on': 'ALEToggle'}
" Use release branch (recommended)
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Track the engine.
Plug 'SirVer/ultisnips'
"
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'

Plug 'maximbaz/lightline-ale'

Plug 'dhruvasagar/vim-table-mode'
Plug 'vim-scripts/DrawIt'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-obsession'

" Plug 'frazrepo/vim-rainbow'
Plug 'junegunn/rainbow_parentheses.vim'

" Plug 'kaarmu/typst.vim'
Plug 'habamax/vim-asciidoctor'

" Plug 'maxboisvert/vim-simple-complete'

Plug 'zah/nim.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'itchyny/screensaver.vim'
Plug 'wellle/context.vim'
Plug 'instant-markdown/vim-instant-markdown'
Plug 'mbbill/undotree'
Plug 'Konfekt/FastFold'

Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'

Plug 't9md/vim-textmanip'
" LSP
" attempt 2
" Plug 'jayli/vim-easycomplete'
Plug 'ntpeters/vim-better-whitespace'
Plug 'mechatroner/rainbow_csv'
Plug 'powerman/vim-plugin-AnsiEsc'

"Plug 'obcat/vim-hitspop' " show number of search matches
" Plug 'haya14busa/is.vim' " hides the search highlighting after moving

Plug 'ofthomas/vim-clippy'
" Plug 'ajatkj/vim-qotd'
" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting

set encoding=utf8

set number
set spell spelllang=en_gb
set tw=80

set background=dark " set dark mode

" colorscheme oli-uwu
" colorscheme oli-fdispatch
" colorscheme retrobox
" colorscheme gruvbox
colorscheme greenisgood

filetype plugin indent on
" show existing tabs with 4 spaces width
set tabstop=4
" when indenting > use 4 spaces
set shiftwidth=4
" tab inserts 4 spaces
set expandtab

" vimtex
"
let g:vimtex_syntax_enable = 1
let g:vimtex_complete_enabled = 1
let g:vimtex_compiler_latexmk = {
            \ 'options' : [
                \   '-pdf',
                \   '-shell-escape',
                \   '-verbose',
                \   '-file-line-error',
                \   '-synctex=1',
                \   '-interaction=nonstopmode',
                \ ],
                \}
let g:tex_flavor='latex'
let g:vimtex_view_method = 'zathura'
let g:vimtex_quickfix_method = 'pplatex'
" let g:vimtex_quickfix_method = 'pulp'

set clipboard=unnamedplus
let g:context_filetype_blacklist = ['tex']
"Auto run

" Start NERDTree when Vim is started without file arguments.
" if empty vim window is opened
autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"close vim if window manager is only window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeWinSize=18

let g:NERDTreeQuitOnOpen = 1
" no spell check for asm
autocmd FileType txt setlocal spell

" spell highlighting
hi clear SpellBad
hi SpellBad cterm=underline

hi clear SpellCap
hi SpellCap cterm=underline

hi clear SpellLocal
hi SpellLocal cterm=underline

hi clear SpellRare
hi SpellRare cterm=underline

" easy tags
let g:easytags_async = 1
let g:easytags_include_members = 1
let g:easytags_always_enabled = 1
"set tags=./.tags;,~/.vimtags
set tags=./tags;
let g:easytags_dynamic_files = 1
" let g:easytags_dynamic_files = 2
let g:easytags_opts = ['--fields=+l']
" let g:easytags_events = ['BufWritePost']

" cmake
let g:cmake_build_executor = 'job'
let g:make_arguments = '-j8'

"templates
let g:tmpl_search_paths = ['~/dotfiles/vim_templates']

" session
let g:session_autosave_periodic=1

au BufNewFile,BufRead *.nim set filetype=nim
au BufNewFile,BufRead *.typ set filetype=markdown

" vim airline
" status line settings
set noshowmode
            "\ 'colorscheme': 'wombat',
            "\ 'colorscheme': 'one',
            "\ 'colorscheme': 'OldHope',

let g:lightline = {
\ 'active': {
\   'left': [ [ 'mode', 'paste', 'modified' ],
\             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
\   'right': [ [ 'lineinfo' ],
\              [ 'percent' ],
\               [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ],
\		[ '%{coc#status()}'],
\               ],
\ },
\ 'component_function': {
\   'lineinfo': 'MyLineinfo',
\   'gitbranch': 'FugitiveHead',
\   'filename': 'LightlineFilename',
\ },
\ }

let g:lightline.component_expand = {
      \  'linter_checking': 'lightline#ale#checking',
      \  'linter_infos': 'lightline#ale#infos',
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \  'linter_ok': 'lightline#ale#ok',
      \ }

let g:lightline.component_type = {
\     'linter_checking': 'right',
\     'linter_infos': 'right',
\     'linter_warnings': 'warning',
\     'linter_errors': 'error',
\     'linter_ok': 'right',
\ }

function! MyLineinfo()
" return printf('%03d/%03d', line('.'),  line('$'))
return line('.') . '/' . line('$') . ':'. col('.')
endfunction

function! LightlineFilename()
  let root = fnamemodify(get(b:, 'git_dir'), ':h')
  let path = expand('%:p')
  if path[:len(root)-1] ==# root
    return path[len(root)+1:]
  endif
  return expand('%')
endfunction

augroup MyGutentagsStatusLineRefresher
            autocmd!
            autocmd User GutentagsUpdating call lightline#update()
            autocmd User GutentagsUpdated call lightline#update()
augroup END

" fzf
function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction

command! ProjectFiles execute 'Files' s:find_git_root()

let g:fzf_preview_window = ['right:50%', 'ctrl-/']

" keybinds
map <C-_> :NERDTreeToggle<CR>
map <c-f> :ProjectFiles<cr>
nmap <c-s> :Lines<CR>
"Tagbartoggle
"nmap <c-s> :TagbarToggle<CR>

map <F4> :e %:p:s,.hpp$,.X123X,:s,.cpp$,.hpp,:s,.X123X$,.cpp,<CR>

map <F5> :CMake<CR>
map <F6> :CMakeBuild<CR>
map <F7> :CMakeRun<CR>
" Apply YCM FixIt
map <F9> :YcmCompleter FixIt<CR>

map <F7> :w<CR>:Gcd <CR>:ter nimble test <CR>
map <F8> :w<CR>:ter nim c -r "%" <CR>
map <F9> :w<CR>:ter nim cpp -r "%" <CR>

map <F10> :w<CR>:! typst compile "%" <CR><CR>

map <F2> :UndotreeToggle<CR>

" commentary
autocmd FileType c,cpp,cs,java setlocal commentstring=//\ %s

""rainbow
let g:rainbow_active = 1
"set redrawtime=10000

" 11 is yellow
" 15 is white
" 121 is lime
" 224 is light white
let g:rainbow#blacklist = [11,15, 224, 121]
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]
" Activation based on file type
augroup rainbow_lisp
  autocmd!
  autocmd FileType * RainbowParentheses
augroup END


" use Rpdf on.pdf to view in vim
:command! -complete=file -nargs=1 Rpdf :r !pdftotext -nopgbrk <q-args> - |fmt -csw78

" vim-tables
let g:table_mode_corner='|'

let g:instant_markdown_autostart = 0

let g:pets_worlds = get(g:, 'pets_worlds', [])
call add(g:pets_worlds, 'ocean')

let g:pets_default_pet = "cat"
let g:pets_garden_width = 19
let g:pets_garden_height = 5

map <c-p> :PetsJoin cat<CR>

let g:ale_completion_enabled = 1
let g:ale_floating_preview = 1
" let g:ale_open_list = 1
let g:ale_list_window_size = 3

" let g:ale_set_loclist = 0
" let g:ale_set_quickfix = 1

let g:ale_patten_options_enabled = 1
let g:ale_patten_options = {
\ '*.yaml$': {'ale_enabled': 0},
\}

" let g:ale_disable_lsp = 1

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" function! Clippy(msg)
"     call clippy#show([a:msg])
" endfunction

" nmap Q :call clippy#show(['Did you mean :q?']) <CR>
" nmap g/ :call ClippyErrors() <CR>
" " nmap gq :call Clippy(qotd#getquoteoftheday()) <CR>

" call timer_start(300, function('clippy#ClippyErrors'), {'repeat': -1})

nmap gs :call system('nerd-dictation begin &') <CR> o
nmap ge :call system('nerd-dictation end') <CR>

" let g:copilot_filetypes = {'markdown': v:true}

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

let g:NERDTreeWinPos = "right"
let NERDTreeShowHidden=1

" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

xmap <C-j> <Plug>(textmanip-move-down)
xmap <C-k> <Plug>(textmanip-move-up)
xmap <C-h> <Plug>(textmanip-move-left)
xmap <C-l> <Plug>(textmanip-move-right)

" toggle insert/replace with <F10>
" nmap <F10> <Plug>(textmanip-toggle-mode)
" xmap <F10> <Plug>(textmanip-toggle-mode)
"
" " use allow key to force replace movement
" xmap  <Up>     <Plug>(textmanip-move-up-r)
" xmap  <Down>   <Plug>(textmanip-move-down-r)
" xmap  <Left>   <Plug>(textmanip-move-left-r)
" xmap  <Right>  <Plug>(textmanip-move-right-r)

let mapleader = " "

set ignorecase
set smartcase
set incsearch

set undodir=~/.vim/undodir
" let &t_TI = ""
" let &t_TE = ""

set t_TI=
set t_TE=
" highlight 80th column
set cc=80
set updatetime=250
set mmp=5000

" search
set hlsearch
let g:hitspop_line   = 'winbot'
let g:hitspop_column = 'winright'

" focus mode
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'

nnoremap <Leader>l :let &scrolloff=999-&scrolloff<CR> :Limelight!!<CR>


" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved
set signcolumn=yes

" Coc detail
hi Question ctermfg=121 gui=bold guifg=Green
hi CocVirtualText ctermfg=121
hi CocHintVirtualText ctermfg=121
hi CocInlayHint ctermfg=121
hi CocSearch ctermfg=121
" hi CocInlayHint ctermfg=29 ctermbg=16 guifg=#326739 guibg=#0e1111
hi MatchParen ctermfg=15 ctermbg=29 guifg=White guibg=#19341d

" nvim hi settings
" hi CocInlayHint ctermfg=15 ctermbg=29 guifg=White guibg=#19341d
hi CocInlayHint ctermfg=12 guifg=#87afd7 " guifg=NvimLightBlue

autocmd BufWritePost $MYVIMRC nested source $MYVIMRC



" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
let g:UltiSnipsExpandTrigger = "<nop>"
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ CheckBackspace() ? "\<TAB>" :
      \ coc#refresh()

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'
" put in :CocConfig
" "ltex.language": "en-GB"
let g:coc_global_extensions = ['coc-sh', 'coc-git', 'coc-clangd', 'coc-pyright', 'coc-calc', 'coc-cmake', 'coc-explorer', 'coc-json', 'coc-markdownlint', 'coc-snippets', 'coc-ltex']

" Use <c-space> to trigger completion
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s)
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying code actions to the selected code block
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying code actions at the cursor position
nmap <leader>ac  <Plug>(coc-codeaction-cursor)
" Remap keys for apply code actions affect whole buffer
nmap <leader>as  <Plug>(coc-codeaction-source)
" Apply the most preferred quickfix action to fix diagnostic on the current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Remap keys for applying refactor code actions
nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

" Run the Code Lens action on the current line
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

let g:coc_filetype_map = {'tex': 'latex'}

set tabstop=8
set shiftwidth=8
set autoindent
set noexpandtab


