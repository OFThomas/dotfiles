ln -sf ~/dotfiles/vimrc ~/.vimrc
ln -sf ~/dotfiles/bashrc ~/.bashrc
rm -rf ~/.vim/colors
ln -sf ~/dotfiles/colors ~/.vim/
ln -sf ~/dotfiles/tmux.conf ~/.tmux.conf
