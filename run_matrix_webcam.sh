######################################################################
# @author      : oli (oli@oli-desktop)
# @file        : run_matrix_webcam
# @created     : Sunday Oct 02, 2022 22:16:14 BST
#
# @description : 
######################################################################

var=$(xdotool getactivewindow)
echo "yep"${var}
wmctrl -i -r ${var} -e 0,300,300,1280,720
matrix-webcam
gst-launch-1.0 ximagesrc xid=${var} ! video/x-raw,framerate=30/1 ! videoconvert ! video/x-raw,format=YUY2 ! v4l2sink device=/dev/video42


