-- ltex dictionary
-- touch ~/.config/nvim/en.utf-8.add
vim.opt.spellfile = vim.fn.stdpath("config") .. "/en.utf-8.add"
local words = {}
for word in io.open(vim.fn.stdpath("config") .. "/en.utf-8.add", "r"):lines() do
	table.insert(words, word)
end

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		vim.api.nvim_echo({
			{ "Failed to clone lazy.nvim:\n", "ErrorMsg" },
			{ out, "WarningMsg" },
			{ "\nPress any key to exit..." },
		}, true, {})
		vim.fn.getchar()
		os.exit(1)
	end
end
vim.opt.rtp:prepend(lazypath)

-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
-- This is also a good place to setup other settings (vim.opt)
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

-- Setup lazy.nvim
require("lazy").setup({
	spec = {
		-- add your plugins here
		"nvim-treesitter/nvim-treesitter",
		{
			"neovim/nvim-lspconfig",
			-- LSPs
			config = function()
				require'lspconfig'.clangd.setup{}	-- requires clangd
				require'lspconfig'.bashls.setup{}	-- bash-language-server
				require'lspconfig'.texlab.setup{}
				require'lspconfig'.eslint.setup{}
				require'lspconfig'.ts_ls.setup{}
				require'lspconfig'.quick_lint_js.setup{}
				require'lspconfig'.jsonls.setup{}
				require'lspconfig'.pyright.setup{}
				-- require'lspconfig'.golangci_lint_ls.setup{}
				require'lspconfig'.ltex.setup{		-- ltex-ls
					settings = {
						ltex = {
							language = "en-GB",
							dictionary = {["en-GB"] = words},
						},
					}
				} 
			end
		},
		"nvim-lua/lsp-status.nvim",
		{
			"nvimdev/epo.nvim",
			config = function()
				require('epo').setup()
			end
		},
		{
			"ray-x/lsp_signature.nvim",
			config = function()
				require'lsp_signature'.setup{}
			end
		},
		{
			"williamboman/mason.nvim",
			config = function()
				require"mason".setup()
			end
		},
		{
			"smjonas/inc-rename.nvim",
			config = function()
				require("inc_rename").setup()
			end,
			vim.keymap.set("n", "<leader>rn", ":IncRename ")
		},
		{
			'nvim-lua/plenary.nvim',
			'nvim-pack/nvim-spectre',
			config = function()
				require('spectre').setup()
			end
		},
		{
			'nvim-telescope/telescope.nvim', tag = '0.1.8',
			dependencies = { 'nvim-lua/plenary.nvim' },
			config = function()
				require('telescope').setup({
					defaults = {
						layout_strategy = "vertical",
						layout_config = {
							vertical = {
								width = 0.9,
								height = 0.9,
								preview_height = 0.5,
								preview_cutoff = 0
							}
						},
					},
				})
			end
		},
		{
			'nvim-telescope/telescope-fzf-native.nvim', build = 'make',
			config = function()
				require('telescope').load_extension('fzf')
			end
		},
		{
			"lervag/vimtex",
			lazy = false,     -- we don't want to lazy load VimTeX
			-- tag = "v2.15", -- uncomment to pin to a specific release
			init = function()
				-- VimTeX configuration goes here, e.g.
				vim.g.vimtex_view_method = "zathura"
			end
		},
		{
			'nvim-lualine/lualine.nvim',
			dependencies = { 'nvim-tree/nvim-web-devicons' },
			config = function()
				require('lualine').setup{}
			end
		},
		{
			"j-hui/fidget.nvim",
			opts = {
				-- options
			},
		},
		{
			'MeanderingProgrammer/render-markdown.nvim',
			opts = {},
			dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' }, -- if you prefer nvim-web-devicons
		},
		{
			"ofthomas/lsp_lines",
			config = function()
				require("lsp_lines").setup()
			end,
		},
		{
			"kylechui/nvim-surround",
			version = "*", -- Use for stability; omit to use `main` branch for the latest features
			event = "VeryLazy",
			config = function()
				require("nvim-surround").setup({
					-- Configuration here, or leave empty to use defaults
				})
			end
		},
		{
			'nvimdev/dashboard-nvim',
			event = 'VimEnter',
			config = function()
				require('dashboard').setup {
					-- config
				}
			end,
			dependencies = { {'nvim-tree/nvim-web-devicons'}}
		},
		{
			-- "andersevenrud/nvim_context_vt"
			"nvim-treesitter/nvim-treesitter-context"
		},
		{
			"folke/which-key.nvim",
			event = "VeryLazy",
			opts = {
				-- your configuration comes here
				-- or leave it empty to use the default settings
				-- refer to the configuration section below
			},
			keys = {
				{
					"<leader>?",
					function()
						require("which-key").show({ global = false })
					end,
					desc = "Buffer Local Keymaps (which-key)",
				},
			},
		},
		{
			'mhinz/vim-signify',
		},
		{
			"NeogitOrg/neogit",
			dependencies = {
				"nvim-lua/plenary.nvim",         -- required
				"sindrets/diffview.nvim",        -- optional - Diff integration
				-- Only one of these is needed, not both.
				"nvim-telescope/telescope.nvim", -- optional
			},
			config = true
		},
		{
			"andrewferrier/debugprint.nvim",
			dependencies = {
				"echasnovski/mini.nvim" -- Needed for :ToggleCommentDebugPrints
			},
			config = function()
				opts = { … }
				require("debugprint").setup(opts)
			end
		},
		{
			"axelf4/vim-strip-trailing-whitespace",
		},
	},
	-- Configure any other settings here. See the documentation for more details.
	-- colorscheme that will be used when installing plugins.
	install = { colorscheme = { "habamax" } },
	-- automatically check for plugin updates
	-- checker = { enabled = true },
})


vim.opt.termguicolors = true

vim.cmd [[
	set clipboard+=unnamedplus
	set number
	set cc=80
	set tw=80
	set ignorecase
	set smartcase
	set incsearch
	set tabstop=8
	set shiftwidth=8
	set autoindent
	set noexpandtab
	"set lcs+=space:·
	set list
	let g:c_syntax_for_h = 1
	let g:c_syntax_for_h = 1
	autocmd FileType c,cpp,cs,java,kotlin setlocal commentstring=//\ %s
	" Return to last edit position when opening files (You want this!)
	autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\   exe "normal! g`\"" |
	\ endif
]]

-- for lsp_lines
vim.diagnostic.config({ virtual_text = false })

local builtin = require('telescope.builtin')
function fuzzyFindFiles()
  builtin.grep_string({
    path_display = { 'smart' },
    only_sort_text = true,
    word_match = "-w",
    search = '',
  })
end
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', '<cmd>lua fuzzyFindFiles{}<cr>', {})
vim.keymap.set('n', '<leader>fw', builtin.grep_string, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

vim.keymap.set('n', 'gd', function()
	vim.lsp.buf.definition({ on_list = on_list })
end, bufopts)

vim.keymap.set('n', 'qf', function()
	vim.lsp.buf.code_action({apply = true})
end, bufopts)


