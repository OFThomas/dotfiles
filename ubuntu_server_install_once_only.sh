######################################################################
# @author      : oli (oli@s3rv3r)
# @file        : ubuntu_server_install_once_only
# @created     : Saturday Nov 12, 2022 22:50:05 GMT
#
# @description : 
######################################################################

sudo apt-get update
sudo apt-get install -y openssh-server 
sudo systemctl enable ssh --now
sudo systemctl start ssh

#enable power button to turn off server LOL
sudo bash -c 'cat > /etc/acpi/events/power' << EOF
event=button/power
action=/sbin/poweroff
EOF


