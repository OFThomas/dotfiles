#! /bin/sh
#
# colors_bak.sh
# Copyright (C) 2021 oli <oli@oli-HP>
#
# Distributed under terms of the MIT license.
#


cp -r ~/.vim/colors ./colors/
cp ~/.vimrc ./vimrc
