######################################################################
# @author      : oli (oli@oli-HP)
# @file        : install
# @created     : Friday Nov 11, 2022 13:43:58 GMT
#
# @description : 
######################################################################
sudo cp -r ./wemux /usr/local/share/
sudo ln -sf /usr/local/share/wemux/wemux /usr/local/bin/wemux
sudo cp /usr/local/share/wemux/wemux.conf.example /usr/local/etc/wemux.conf

echo "host_list=($@)" | sudo tee -a /usr/local/etc/wemux.conf
echo 'options="-2 -u"' | sudo tee -a /usr/local/etc/wemux.conf

sudo apt-get update
sudo apt-get install tmux -y



