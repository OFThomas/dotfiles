" Author        : oli
" Created       : 15/08/2023
" License       : MIT
" Description   : 

function! myspacevim#before() abort 
  call SpaceVim#custom#SPCGroupName(['c'], '+Compile')
  call SpaceVim#custom#SPC('nnoremap', ['c', 'b'], ':CMake build', 'CMake-Build', 1)    
  call SpaceVim#custom#SPC('nnoremap', ['c', 'r'], ':CMake build_and_run', 'CMake-Build&Run', 1)    
  let g:everforest_background = 'hard'
endfunction

let g:markdown_fenced_languages = ['go','perl','sh', 'bash', 'python', 'cpp', 'c', 'javascript', 'vim']

